#!/usr/bin/env python3
import os

import uvicorn
from testcontainers.postgres import PostgresContainer

if __name__ == "__main__":
    print(
        """
        *************** WARNING ***************
    This script is for testing purposes
    You should not run it in any kind of meaningful deployment
    Probably shouldn't even run it in CI
    Keep it to local testing
        ************* END WARNING *************
    """
    )

    with PostgresContainer("postgres:13-alpine") as c:
        os.environ["DATABASE_FULL_URL"] = c.get_connection_url()

        uvicorn.run("py56:fifty_six", reload=True)
