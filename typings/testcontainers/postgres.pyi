from contextlib import AbstractContextManager
from typing import Optional

class PostgresContainer(AbstractContextManager):
    def __init__(
        self,
        image: str = ...,
        port: int = ...,
        user: Optional[str] = ...,
        password: Optional[str] = ...,
        dbname: Optional[str] = ...,
    ) -> None: ...
    def get_connection_url(self) -> str: ...
