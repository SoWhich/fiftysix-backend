class CryptoError(Exception): ...
class BadSignatureError(CryptoError): ...

# class RuntimeError(RuntimeError, CryptoError): ...
# class AssertionError(AssertionError, CryptoError): ...
# class TypeError(TypeError, CryptoError): ...
# class ValueError(ValueError, CryptoError): ...
class InvalidkeyError(CryptoError): ...
class CryptPrefixError(InvalidkeyError): ...
class UnavailableError(RuntimeError): ...
