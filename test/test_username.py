from test.util.to_json import to_json
from test.util.wrap_container import WrapContainer

from fastapi.testclient import TestClient


def test_username_taken():
    with WrapContainer() as app:

        tc = TestClient(app)
        is_user_json = to_json(tc.get("/is_user/somebody"))
        assert not is_user_json["found_user"]

        create_user_json = to_json(tc.post("/create_user", json={"uname": "somebody"}))

        assert create_user_json["added"]

        is_user_json = to_json(tc.get("/is_user/somebody"))
        assert is_user_json["found_user"]

        is_user_json = to_json(tc.get("/is_user/SomeBody"))
        assert is_user_json["found_user"]

        is_user_json = to_json(tc.get("/is_user/sOmEBoDY"))
        assert is_user_json["found_user"]

        create_user_json = to_json(tc.post("/create_user", json={"uname": "SomeBody"}))
        assert not create_user_json["added"]

        create_user_json = to_json(tc.post("/create_user", json={"uname": "sOMeBOdy"}))
        assert not create_user_json["added"]


def test_forbid_invalid_unames():
    with WrapContainer() as app:

        tc = TestClient(app)

        create_user_json = to_json(tc.post("/create_user", json={"uname": "sml"}))
        assert not create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "has whitespace"})
        )
        assert not create_user_json["added"]

        create_user_json = to_json(tc.post("/create_user", json={"uname": "hasEmoji💩"}))
        assert not create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "1thatStartsWithNumber"})
        )
        assert not create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "-startsWithDash"})
        )
        assert not create_user_json["added"]


def test_permit_valid_unames():
    with WrapContainer() as app:

        tc = TestClient(app)

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "longenough"})
        )
        assert create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "vaRiedCasE"})
        )
        assert create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "hasNumbers152"})
        )
        assert create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "has-dash_and-scores"})
        )
        assert create_user_json["added"]

        create_user_json = to_json(
            tc.post("/create_user", json={"uname": "numb3rs_n0t_at_3nd"})
        )
        assert create_user_json["added"]
