import json
from test.util.to_json import to_json
from test.util.wrap_container import WrapContainer
from typing import List

from fastapi import FastAPI
from fastapi.testclient import TestClient


def before_each(app: FastAPI, count: int = 6) -> List[TestClient]:
    from sqlalchemy.sql import insert

    import py56.database.tables as tb
    from py56.database.engine import transaction

    def make_client(ind: int) -> TestClient:
        client = TestClient(app)
        client.post("/create_user", json={"uname": f"player_{ind}"})
        return client

    with transaction() as c:
        c.execute(
            insert(tb.NameParts).values(
                [
                    ("good", 0),
                    ("nice", 0),
                    ("clever", 0),
                    ("smart", 0),
                    ("freaking", 1),
                    ("stinking", 1),
                    ("dang", 1),
                    ("game", 2),
                    ("thing", 2),
                    ("whatsit", 2),
                    ("play", 2),
                    ("whatever", 2),
                ]
            )
        )

    return [make_client(player) for player in range(count)]


def test_happy_path() -> None:
    with WrapContainer() as app:
        clients = before_each(app)

        res = clients[0].post("/play/reserve", {"players": 6})
        room_name = json.loads(res.content)["room_name"]
        for tn, client in enumerate(clients):
            assert client.post(f"/play/join/{room_name}/{tn % 2}").status_code == 200
            assert client.post(f"/play/ready/{room_name}").status_code == 200

        assert to_json(clients[0].get("f/play/startable/{room_name}"))["ready"]
        assert clients[0].post(f"/play/start/{room_name}").status_code == 200


def test_dont_start_without_matching_player_count() -> None:
    with WrapContainer() as app:
        clients = before_each(app, 4)

        res = clients[0].post("/play/reserve", {"players": 6})
        room_name = json.loads(res.content)["room_name"]
        for tn, client in enumerate(clients):
            assert client.post(f"/play/join/{room_name}/{tn % 2}").status_code == 200
            assert client.post(f"/play/ready/{room_name}").status_code == 200

        res = clients[0].post(f"/play/start/{room_name}")
        assert res.status_code == 409


def test_dont_start_without_even_teams() -> None:
    with WrapContainer() as app:
        clients = before_each(app)
        res = clients[0].post("/play/reserve", {"players": 6})
        room_name = json.loads(res.content)["room_name"]
        for tn, client in enumerate(clients):
            assert client.post(f"/play/join/{room_name}/{tn % 3 % 2}").status_code == 200
            assert client.post(f"/play/ready/{room_name}").status_code == 200

        res.clients[0].post(f"/play/start/{room_name}")
        assert res.status_code == 409


def test_room_creator_must_start_game() -> None:
    with WrapContainer() as app:
        clients = before_each(app)

        res = clients[0].post("/play/reserve", {"players": 6})
        room_name = json.loads(res.content)["room_name"]
        for tn, client in enumerate(clients):
            assert client.post(f"/play/join/{room_name}/{tn % 2}").status_code == 200
            assert client.post(f"/play/ready/{room_name}").status_code == 200

        assert clients[1].post(f"/play/start/{room_name}").status_code == 403


def test_every_player_must_mark_ready() -> None:
    with WrapContainer() as app:
        clients = before_each(app)

        res = clients[0].post("/play/reserve", {"players": 6})
        room_name = json.loads(res.content)["room_name"]
        for tn, client in enumerate(clients):
            assert client.post(f"/play/join/{room_name}/{tn % 2}").status_code == 200
            if tn != 4:
                assert client.post(f"/play/ready/{room_name}").status_code == 200

        assert clients[1].post(f"/play/start/{room_name}").status_code == 409
