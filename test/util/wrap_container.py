from contextlib import AbstractContextManager
from os import environ

from fastapi import FastAPI
from testcontainers.postgres import PostgresContainer

from py56.config import connect_string_key


class WrapContainer(AbstractContextManager):
    def __init__(self) -> None:
        self.container = PostgresContainer("postgres:13-alpine")
        self.old_full_url = environ.get(connect_string_key)

    def __enter__(self) -> FastAPI:
        self.container.__enter__()
        environ[connect_string_key] = self.container.get_connection_url()

        import py56
        from py56.database.engine import refresh_engine

        refresh_engine()

        return py56.fifty_six

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.container.__exit__(exc_type, exc_val, exc_tb)
        if self.old_full_url:
            environ[connect_string_key] = self.old_full_url
        else:
            del environ[connect_string_key]
