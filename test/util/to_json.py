# type: ignore
import json


def to_json(response):
    return json.loads(response.content)
