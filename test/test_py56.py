from test.util.wrap_container import WrapContainer


def test_version():
    with WrapContainer() as _:
        from py56 import __version__

        assert __version__ == "0.0.1"
