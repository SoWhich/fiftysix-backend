from test.util.to_json import to_json
from test.util.wrap_container import WrapContainer

from fastapi.testclient import TestClient


def test_valid_login():
    with WrapContainer() as app:
        tc = TestClient(app)
        login_data = {"uname": "matthew", "password": "123456"}
        tc.post("/create_user", json=login_data)
        login_json = to_json(tc.post("/login", json=login_data))
        assert login_json["ok"]

        verify = to_json(tc.get("/play"))
        assert verify["uname"] == login_data["uname"]


def test_must_use_own_password():
    with WrapContainer() as app:
        tc = TestClient(app)
        tc.post("/create_user", json={"uname": "matthew", "password": "123456"})
        tc.post("/create_user", json={"uname": "mahhtew", "password": "654321"})

        res = tc.post("/login", json={"uname": "matthew", "password": "654321"})
        assert to_json(res)["error"] and res.status_code == 401

        res = tc.post("/login", json={"uname": "mahhtew", "password": "123456"})
        assert to_json(res)["error"] and res.status_code == 401

        check_res = tc.get("/play")
        assert check_res.status_code == 401


def test_must_provide_a_username_and_password():
    with WrapContainer() as app:
        tc = TestClient(app)
        tc.post("/create_user", json={"uname": "matthew", "password": "123456"})

        res = tc.post("/login", json={"uname": "matthew"})
        assert to_json(res)["error"] and res.status_code == 400

        res = tc.post("/login", json={"password": "123456"})
        assert res.status_code != 200

        check_res = tc.get("/play")
        assert check_res.status_code == 401
