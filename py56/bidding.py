from dataclasses import dataclass
from typing import List, Optional

from py56.card import Suit
from py56.game import Hand, suits


@dataclass
class Bid:
    suit: Optional[Suit]
    value: int


Bids = List[Optional[Bid]]


def is_legal(attempt: Optional[Bid], hand: Hand, last: Optional[Bid] = None) -> bool:
    if attempt is None:
        return True

    if attempt.suit is not None and attempt.suit not in suits(hand):
        return False

    if attempt.value not in range(28, 57):
        return False

    return last is None or attempt.value > last.value


def is_complete(bids: Bids) -> Optional[Bid]:
    """
    Expects bids to be the length of the number of players, that is that it is
    the (number of players) most recent bids
    """
    if all(bid is None for bid in bids[1:]):
        return bids[0]
    return None
