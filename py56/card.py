from __future__ import annotations

from dataclasses import dataclass
from enum import Enum

"""
For both Suit and Rank it's important that the values be consistent across
multiple possible running instances of the app so they can share data in a
database. Otherwise, Suit would use enum.auto() rather than specifying values.

It is important for readability purposes that you only refer to these through
their named handles, and not by their integer value unless interacting with the
database.
"""


class Suit(Enum):
    Hearts = 0
    Spades = 1
    Diamonds = 2
    Clubs = 3


class Rank(Enum):
    Queen = 1
    King = 2
    Ten = 3
    Ace = 4
    Nine = 5
    Jack = 6

    def __cmp__(self, other: Rank) -> int:
        return self.value - other.value

    @property
    def score(self) -> int:
        if self is Rank.Queen or self is Rank.King:
            return 0
        if self is Rank.Ten or self is Rank.Ace:
            return 1
        if self is Rank.Nine:
            return 2
        if self is Rank.Jack:
            return 3
        raise TypeError("Bad <Rank> value")


@dataclass
class Card:
    rank: Rank
    suit: Suit
