from typing import Dict, List, Set

from py56.card import Card, Suit

Hand = Dict[Card, int]
CardPlays = List[Card]


def suits(hand: Hand) -> Set[Suit]:
    return set(card.suit for card in hand.keys())
