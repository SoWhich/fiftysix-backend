from typing import Iterator, Optional, Protocol, Union

from sqlalchemy import func, sql

import py56.database.tables as tb
import py56.database.types as tp
import py56.util.iterator as iter
from py56.database.engine import transaction

PARTS_LIMIT = 3


class ColumnAlias(Protocol):
    def __add__(self, rhs: Union["ColumnAlias", str]) -> "ColumnAlias":
        ...

    def label(self, val: str) -> "ColumnAlias":
        ...


def create_room(user: tp.UserId) -> tp.RoomId:
    with transaction() as c:
        parts = (
            sql.select(tb.NameParts.word).where(tb.NameParts.position == pos).alias()
            for pos in range(PARTS_LIMIT)
        )

        words = (part.c.word for part in parts)

        combos = (
            sql.select(interleaved(words).label("roomname"))
            .order_by(sql.expression.func.random())
            .alias()
        )

        first_unused = (
            sql.select(combos.c.roomname)
            .join(tb.Game, combos.c.roomname == tb.Game.game, isouter=True)
            .where(tb.Game.game.is_(None))
            .limit(1)
        )

        (row,) = c.execute(
            sql.insert(tb.Game).values(
                game=first_unused.scalar_subquery(), creator=user
            )
        ).inserted_primary_key

        return tp.RoomId(row)


def join_room(
    user: tp.UserId,
    room: tp.RoomId,
    team: tp.TeamId,
) -> None:
    with transaction() as c:
        c.execute(
            sql.insert(tb.Player).values(game=room, uuid=user, nick=user, team=team)
        )


def select_team(user: tp.UserId, room: tp.RoomId, team: tp.TeamId) -> None:
    with transaction() as c:
        # TODO: guard this better
        assert c.execute(
            sql.select(tb.Game).where(
                tb.Game.game == room, tb.Game.phase == tp.Phase.pregame
            )
        ).fetchone()
        c.execute(
            sql.update(tb.Player)
            .values(team=team)
            .where(
                tb.Player.game == room,
                tb.Player.uuid == user,
            )
        )


def start_game(user: tp.UserId, room: tp.RoomId) -> Optional[str]:
    with transaction() as c:
        tc = (
            c.execute(
                sql.select(tb.Game).where(tb.Game.game == room, tb.Game.creator == user)
            )
            .one()
            .team_count
        )
        player_count = (
            c.execute(
                sql.select(func.count("*"))
                .select_from(tb.Player)
                .where(tb.Player.game == room, tb.Player.team == team)
            ).scalar()
            for team in range(tc)
        )

        assert iter.eq(player_count)

        c.execute(
            sql.update(tb.Game)
            .values(phase=tp.Phase.bidding)
            .where(tb.Game.game == room, tb.Game.creator == user)
        )
    return None


def interleaved(parts: Iterator[ColumnAlias]) -> ColumnAlias:
    first = parts.__next__()
    for part in parts:
        first = first + "-" + part
    return first


def can_start_game(user: tp.UserId, room: tp.RoomId) -> dict:
    return {}
