import uuid
from typing import Awaitable, Callable

from fastapi import FastAPI, Request, Response, status
from fastapi.responses import JSONResponse

import py56.database.types as tp
import py56.play.room as r
import py56.session as session

play = FastAPI()


@play.middleware("http")
async def logged_in_middleware(
    request: Request, call_next: Callable[[Request], Awaitable[Response]]
) -> Response:
    failureResponse = JSONResponse(
        status_code=status.HTTP_401_UNAUTHORIZED,
        headers={},
        content={"err": "cannot access content not logged in"},
    )
    session.set_cookie(failureResponse, None)

    cookie_str = request.cookies.get("session")

    if not cookie_str:
        return failureResponse

    uname = session.logged_in_as(tp.SessionId(uuid.UUID(cookie_str)))

    if not uname:
        return failureResponse

    request.state.username = uname
    return await call_next(request)


@play.get("/")
def logged_in_as(req: Request) -> dict:
    return {"uname": req.state.username}


@play.post("/reserve")
def create_room(req: Request) -> dict:
    return {"room_name": r.create_room(req.state.username)}


@play.post("/join/{room}/{team_no}")
def join_room(req: Request, room: str, team_no: int) -> dict:
    r.join_room(req.state.username, r.tp.RoomId(room), r.tp.TeamId(team_no))
    return {"joined": True}


@play.post("/ready/{room}")
def ready_player(req: Request, room: str) -> dict:
    return {}


@play.post("/startable/{room}")
def can_start_game(req: Request, room: str) -> dict:
    r.can_start_game(req.state.username, r.tp.RoomId(room))
    return {}


@play.post("/start/{room}")
def start_game(req: Request, room: str) -> dict:
    r.start_game(req.state.username, r.tp.RoomId(room))
    return {}
