from __future__ import annotations

import os
from typing import Optional, Protocol


class RODict(Protocol):
    def __getitem__(self, key: str) -> str:
        ...

    def get(self, key: str) -> Optional[str]:
        ...


connect_string_key = "DATABASE_FULL_URL"
username_key = "DATABASE_USER"
password_key = "DATABASE_PASS"
db_url_key = "DATABASE_URL"
db_name_key = "DATABASE_NAME"
frontend_key = "FRONTEND_DOMAIN"


class Config:
    env: RODict

    def __init__(self, env: RODict = os.environ) -> None:
        self.env = env

    def reset(self, env: RODict = os.environ) -> Config:
        self.env = env
        return self

    @property
    def full_url(self) -> str:
        return self.env.get(connect_string_key) or "".join(
            [
                "postgresql+psycopg2://",
                self.env[username_key],
                ":",
                self.env[password_key],
                "@",
                self.env[db_url_key],
                "/",
                self.env[db_name_key],
            ]
        )

    @property
    def frontend_domain(self) -> str:
        return self.env.get(frontend_key) or ""


config = Config()


def make_config(env: RODict = os.environ) -> Config:
    return config.reset(env)
