from typing import Literal, Type, Union, cast

from fastapi import FastAPI, Response, status
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

import py56.passwords as pswd
import py56.play
import py56.session as session
import py56.users as users
from py56.config import config

fifty_six = FastAPI()

fifty_six.add_middleware(
    CORSMiddleware,
    allow_origins=config.frontend_domain,
    allow_credentials=True,
    allow_methods=["DELETE", "GET", "POST", "PUT"],
    allow_headers=["*"],
)

__version__ = "0.0.1"


@fifty_six.get("/")
async def root() -> dict:
    return {"message": "Hello World"}


class IsUser(BaseModel):
    found_user: bool


@fifty_six.get("/is_user/{username}", response_model=IsUser)
def is_user(username: str) -> IsUser:
    user = users.UserId(username)
    if user:
        return IsUser(found_user=users.is_user(user))
    else:
        return IsUser(found_user=False)


class AddFailure(BaseModel):
    rationale: str
    added: Literal[False] = False


class AddSuccess(BaseModel):
    added: Literal[True] = True


AddResponse = Union[AddSuccess, AddFailure]


@fifty_six.post("/create_user", response_model=cast(Type, AddResponse))
def add_user(user: users.UserPass, response: Response) -> AddResponse:
    uid = users.UserId(user.uname)
    if not uid:
        return AddFailure(rationale=f"Invalid username {user.uname}")

    added = users.add_user(
        uid,
        pswd.hash(user.password) if user.password is not None else None,
    )
    if added is None:
        session.set_cookie(response, session.create_session(uid))
        return AddSuccess()
    else:
        return AddFailure(rationale=added)


class LoginFailure(BaseModel):
    error: str
    ok: Literal[False] = False


class LoginSuccess(BaseModel):
    ok: Literal[True] = True


LoginResponse = Union[LoginFailure, LoginSuccess]


@fifty_six.post("/login", response_model=cast(Type, LoginResponse))
async def login(user: users.UserPass, response: Response) -> LoginResponse:
    uid = users.UserId(user.uname)
    response.status_code = status.HTTP_400_BAD_REQUEST
    session.set_cookie(response, None)
    if not uid:
        return LoginFailure(error="invalid username")
    if not user.password:
        return LoginFailure(error="password required")
    login_res = session.login(uid, user.password)
    if not login_res:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return LoginFailure(error="incorrect login information")
    session.set_cookie(response, login_res)
    response.status_code = status.HTTP_200_OK
    return LoginSuccess()


fifty_six.mount("/play", py56.play.play)
