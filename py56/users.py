import re
from datetime import datetime, timezone
from typing import Optional

import sqlalchemy.dialects.postgresql as pgsql
import sqlalchemy.sql as sql
from pydantic import BaseModel
from sqlalchemy.engine import Connection
from sqlalchemy.exc import IntegrityError

import py56.database.tables as tb
import py56.database.types as tp
from py56.database.engine import connection_guard, transaction
from py56.passwords import HashedPass

ValidUsername = re.compile("^[a-zA-Z][a-zA-Z0-9_-]{5,}$")


def UserId(uname: str) -> Optional[tp.UserId]:
    return tp.UserId(uname.lower()) if ValidUsername.match(uname) else None


class UserPass(BaseModel):
    uname: str
    password: Optional[str] = None


def is_user(uname: tp.UserId, conn: Optional[Connection] = None) -> bool:
    statement = sql.select([tb.User]).where(tb.User.uuid == uname)

    def exec(c: Connection) -> bool:
        result = c.execute(statement)
        return bool(result.fetchone())

    return connection_guard(exec, conn)


def add_user(
    uname: tp.UserId,
    pswd: Optional[HashedPass] = None,
) -> Optional[str]:

    stamp = datetime.now(timezone.utc)

    try:
        with transaction() as c:
            insert_stmt = pgsql.insert(tb.User).values(
                uuid=uname,
                display=uname,
                pass_hash=pswd,
                creation_stamp=stamp,
            )
            c.execute(insert_stmt)
        return None
    except IntegrityError:
        return f"Username {uname} already taken"
