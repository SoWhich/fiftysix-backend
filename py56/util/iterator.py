from typing import Iterable, TypeVar

T = TypeVar("T")


def eq(iter: Iterable[T]) -> bool:
    cmpval = None
    isfirst = True
    for val in iter:
        if not isfirst:
            if val != cmpval:
                return False
        else:
            isfirst = False
            cmpval = val
    return True
