from dataclasses import asdict, dataclass
from uuid import UUID
from datetime import datetime, timedelta, timezone
from typing import Optional

import fastapi
import sqlalchemy.sql as sql

import py56.database.tables as tb
import py56.database.types as tp
from py56.database.engine import transaction
from py56.passwords import verify

session_timeout = timedelta(seconds=400000)
session_timeout_seconds = int(session_timeout.total_seconds())


@dataclass
class Cookie:
    key: str
    value: str
    expires: Optional[int] = None


EMPTY_SESSION = Cookie(key="session", value="", expires=-1)


def set_cookie(response: fastapi.Response, cookie: Optional[Cookie]) -> None:
    response.set_cookie(**(asdict(cookie) if cookie else asdict(EMPTY_SESSION)))


def logged_in_as(session: tp.SessionId) -> Optional[tp.UserId]:
    before = datetime.now(timezone.utc) - session_timeout
    with transaction() as c:
        res = c.execute(
            sql.select([tb.Session]).where(
                tb.Session.session_cookie == session,
                tb.Session.last_used >= before,
            )
        ).fetchone()
        return res.uuid if res else None


def login(username: tp.UserId, password: str) -> Optional[Cookie]:
    with transaction() as c:
        valid_login = sql.select([tb.User.pass_hash]).where(tb.User.uuid == username)
        result = c.execute(valid_login).fetchone()
        return (
            create_session(username)
            if result and verify(result.pass_hash, password)
            else None
        )


def create_session(uname: tp.UserId) -> Cookie:
    stamp = datetime.now(timezone.utc)
    with transaction() as c:
        session: UUID = c.execute(
            sql.insert(tb.Session).values(
                uuid=uname,
                created=stamp,
                last_used=stamp,
            )
        ).inserted_primary_key[0]
        return Cookie(key=EMPTY_SESSION.key, value=session.urn)


def refresh_session(session: tp.SessionId) -> bool:
    stamp = datetime.now(timezone.utc)
    with transaction() as c:
        c.execute(
            sql.update(tb.Session).values(session_cookie=session, last_used=stamp)
        )
    return True
