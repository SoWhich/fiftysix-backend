import re
from typing import NewType, Optional

import nacl.pwhash as pw
import nacl.pwhash.argon2id as a2
from nacl.exceptions import InvalidkeyError

HashedPass = NewType("HashedPass", bytes)

ValidPassword = re.compile("")


def hash(pswd: str) -> Optional[HashedPass]:
    if not ValidPassword.match(pswd):
        return None
    as_bytes = pswd.encode("utf-8")
    hashed = a2.str(as_bytes)
    return HashedPass(hashed)


def verify(hashed: HashedPass, pswd: str) -> bool:
    try:
        return pw.verify(hashed, pswd.encode("utf-8"))
    except InvalidkeyError:
        return False
