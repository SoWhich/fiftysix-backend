import uuid

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from sqlalchemy import Column, DateTime, Enum, Integer, String
from sqlalchemy.orm import Mapped, declarative_base
from sqlalchemy.sql.schema import ForeignKey

import py56.database.types as types

metadata = sa.MetaData()
Base = declarative_base(metadata=metadata)


class User(Base):
    __tablename__ = "user"
    uuid = Column(String, primary_key=True)
    display = Column(String)
    creation_stamp = Column(DateTime, nullable=False)
    pass_hash = Column(pg.BYTEA(), nullable=True)


class Session(Base):
    __tablename__ = "session"
    session_cookie: Mapped[uuid.UUID] = Column(
        pg.UUID(as_uuid=True),
        primary_key=True,
        unique=True,
        default=sa.text("gen_random_uuid()"),
    )
    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    created = Column(DateTime, nullable=False)
    last_used = Column(DateTime, nullable=False)


class Game(Base):
    __tablename__ = "game"
    game = Column(String, primary_key=True)
    creator = Column(String, ForeignKey("user.uuid"), nullable=False)
    phase = Column(Enum(types.Phase), default=types.Phase.pregame)
    team_count = Column(Integer, default=2)
    score_balance = Column(Integer, default=0)


class GameName(Base):
    __tablename__ = "gamename"
    __table_args__ = (sa.PrimaryKeyConstraint("user", "game", name="gamename_pk"),)

    user = Column(String, ForeignKey("user.uuid"), nullable=False)
    game = Column(String, ForeignKey("game.game"), nullable=False)
    nick = Column(String, nullable=False)


class LocalName(Base):
    __tablename__ = "localname"
    __table_args__ = (
        sa.PrimaryKeyConstraint("owner", "game", "named", name="localname_pk"),
    )

    owner = Column(String, ForeignKey("user.uuid"), nullable=False)
    game = Column(String, ForeignKey("game.game"), nullable=False)
    named = Column(String, ForeignKey("user.uuid"), nullable=False)
    nick = Column(String, nullable=False)


class LoginLink(Base):
    __tablename__ = "loginlink"

    uuid = Column(String, ForeignKey("user.uuid"), unique=True, nullable=False)
    expiry = Column(sa.DateTime, unique=True, nullable=False)
    url = Column(String, unique=True, nullable=False, primary_key=True)


class Player(Base):
    __tablename__ = "player"
    __table_args__ = (sa.PrimaryKeyConstraint("game", "uuid", name="players_pk"),)

    game = Column(String, ForeignKey("game.game"), nullable=False)
    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    nick = Column(String)
    team = Column(Integer)


class Hand(Base):
    __tablename__ = "hand"
    __table_args__ = (
        sa.PrimaryKeyConstraint("game", "uuid", "rank", "suit", name="hand_pk"),
    )

    game = Column(String, ForeignKey("game.game"), nullable=False)
    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    rank = Column(Integer, nullable=False)
    suit = Column(Integer, nullable=False)
    count = Column(Integer, nullable=False)


class Play(Base):
    __tablename__ = "play"
    __table_args__ = (
        sa.PrimaryKeyConstraint("game", "uuid", "round", name="one_play_per_round"),
        sa.UniqueConstraint(
            "game", "round", "play_number", name="play_in_unique_position"
        ),
    )

    game = Column(String, ForeignKey("game.game"), nullable=False)
    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    round = Column(Integer, nullable=False)
    play_number = Column(Integer, nullable=False)
    suit = Column(Integer, nullable=False)
    rank = Column(Integer, nullable=False)


class Bid(Base):
    __tablename__ = "bid"
    __table_args__ = (
        sa.UniqueConstraint("game", "bid", name="bids_unique_per_game"),
        sa.PrimaryKeyConstraint("game", "bid", "uuid", "suit", name="bids_pk"),
    )

    game = Column(String, ForeignKey("game.game"), nullable=False)
    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    bid = Column(Integer)
    suit = Column(Integer)


class PlayerOrder(Base):
    __tablename__ = "player_order"
    __table_args__ = (sa.PrimaryKeyConstraint("uuid", "game", name="player_order_pk"),)

    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    game = Column(String, ForeignKey("game.game"), nullable=False)
    position = Column(Integer, nullable=False)


class Team(Base):
    __tablename__ = "team"
    __table_args__ = (sa.PrimaryKeyConstraint("uuid", "game", name="team_pk"),)

    uuid = Column(String, ForeignKey("user.uuid"), nullable=False)
    game = Column(String, ForeignKey("game.game"), nullable=False)
    team = Column(Integer, nullable=False)


class Score(Base):
    __tablename__ = "score"
    __table_args__ = (sa.PrimaryKeyConstraint("team", "game", name="score_pk"),)

    game = Column(String, ForeignKey("game.game"), nullable=False)
    team = Column(Integer, nullable=False)
    score = Column(Integer, nullable=False)


class NameParts(Base):
    __tablename__ = "nameparts"
    __table_args__ = (sa.PrimaryKeyConstraint("word", "position", name="nameparts_pk"),)

    word = Column(String, nullable=False)
    position = Column(Integer, nullable=False)
