import enum
from dataclasses import dataclass
from datetime import date
from typing import NewType
from uuid import UUID

from py56.card import Rank, Suit

GameId = NewType("GameId", str)
UserId = NewType("UserId", str)
SessionId = NewType("SessionId", UUID)
Url = NewType("Url", str)
TeamId = NewType("TeamId", int)
RoomId = NewType("Room", str)


class Phase(enum.Enum):
    pregame = "pregame"
    bidding = "bidding"
    playing = "playing"


@dataclass
class User:
    uuid: UserId
    pass_hash: str


@dataclass
class Session:
    session_cookie: SessionId  # foreign key, primary key
    uuid: UserId  # foreign key


@dataclass
class Game:
    game: GameId
    phase: Phase
    team_count: int


@dataclass
class GameName:
    uuid: UserId
    game: GameId
    nick: str


@dataclass
class LocalName:
    owner: UserId
    game: GameId
    player: UserId
    nick: str


@dataclass
class LoginLink:
    uuid: UserId
    expiry: date
    url: Url


@dataclass
class Players:
    # unique
    game: GameId  # foreign key
    user: UserId  # foreign key

    nick: str


@dataclass
class Hand:
    # unique
    game: GameId  # foreign key
    uuid: UserId  # foreign key
    rank: Rank
    suit: Suit

    # not unique
    count: int


@dataclass
class Plays:
    game: GameId
    player: UserId
    round: int
    play_number: int
    suit: Suit
    rank: Rank

    # unique: GameId, UserId, round
    # unique: GameId, round, play_number


@dataclass
class Bids:
    # unique
    game: GameId
    bid: int  # Nullable

    player: UserId
    suit: Suit  # Nullable


@dataclass
class PlayerOrder:
    player: UserId  # foreign key
    game: GameId  # foreign key
    position: int
    # unique: game, position
    # unique: player, game


@dataclass
class Team:
    game: GameId
    player: UserId
    team: TeamId
    # unique pk: game, player


@dataclass
class Score:
    game: GameId
    team: TeamId
    score: int
