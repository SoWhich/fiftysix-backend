from typing import Callable, Optional, TypeVar

from sqlalchemy import create_engine
from sqlalchemy.engine import Connection, Engine

from py56.config import make_config
from py56.database.tables import metadata

__engine: Engine


def refresh_engine() -> None:
    global __engine
    __engine = create_engine(url=make_config().full_url, echo=True)
    metadata.create_all(__engine)


def transaction() -> Engine._trans_ctx:
    global __engine
    try:
        return __engine.begin()
    except NameError:
        refresh_engine()
        return __engine.begin()


T = TypeVar("T")


def connection_guard(func: Callable[[Connection], T], conn: Optional[Connection]) -> T:
    if conn:
        return func(conn)
    with transaction() as c:
        return func(c)
